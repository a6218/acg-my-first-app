import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-logs',
  templateUrl: './logs.component.html',
  styleUrls: ['./logs.component.scss']
})
export class LogsComponent implements OnInit {
  logList: Array<Date> = [];
  showDetails: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  displayDetails(): void {
    this.showDetails = !this.showDetails;
    this.logList.push(new Date());
  }

  setBackground(logNumber: string): string {
    console.log(typeof Number(logNumber));
    if (Number(logNumber) < 4) {
      return '';
    }

    return 'blue';
  }

  setColor(logNumber: string): boolean {
    if (Number(logNumber) < 4) {
      return false;
    }

    return true;;
  }
}
