import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-servers',
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.scss']
})
export class ServersComponent implements OnInit {
  allowNewServer: boolean = false;
  serverCreationStatus: string = 'No server created!!!';
  serverCreationName: string = 'Testserver';
  username: string = '';
  allowSave: boolean = false;
  serverCreated: boolean = false;
  servers: Array<string> = [
    'Testserver', 'Testserver 2'
  ];

  constructor() {
    setTimeout(() => {
      this.allowNewServer = true;
    }, 2000);
  }

  ngOnInit(): void {
  }

  onCreateServer() {
    this.serverCreated = true;
    if (this.serverCreationName) {
      this.servers.push(this.serverCreationName);
    }
    this.serverCreationStatus = 'Sever was created!!!';
  }

  onUpdateServerName(event: Event): void {
    const value: string = (<HTMLInputElement>event.target).value;
    this.serverCreationName = value;
  }

  usernameIsEmpty(): boolean {
    return this.username.trim() === '';
  }

  onSave(): void {
    this.username = '';
  }
}
