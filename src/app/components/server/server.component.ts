import { Component, OnInit } from '@angular/core';

const enum ServerStatusEnum {
  online = "online",
  offline = "offline"
};

@Component({
  selector: 'app-server',
  templateUrl: './server.component.html',
  styleUrls: ['./server.component.scss']
})

export class ServerComponent implements OnInit {
  serverId: number = 10;
  serverStatus: string = ServerStatusEnum.offline;

  constructor() { }

  ngOnInit(): void {
    this.setRandomServerStatus();
  }

  setRandomServerStatus(): void {
    this.serverStatus = Math.random() > 0.5 ? ServerStatusEnum.online : ServerStatusEnum.offline;    
  }

  getServerColor(): string {
    if (this.serverStatus === ServerStatusEnum.offline) {
      return 'red';
    }

    return 'green';
  }

  setOnlineClass():boolean {
    if (this.serverStatus === ServerStatusEnum.online) {
      return true;
    }
    return false;
  }

  showServerStatus(): string {
    return this.serverStatus;
  }
  
}
